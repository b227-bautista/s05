@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>	
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
						  Comment
						</button>
				</form>
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
		
	</div>

	<!-- Button trigger modal -->
	

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Post a Comment</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	        Content:
	        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
	        	@method('PUT')
	        	@csrf
	        	<textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
	        	<button type="submit" class="btn btn-primary me-auto mt-3">Post Comment</button>
	        	</form>
	      </div>
	    </div>
	  </div>
	</div>

	<h4 class="mt-5">Comments:</h4>
	@foreach($post->comments as $comment)
		<div class="card mb-2">
			<div class="card-body">
				<h5 class="text-center">{{$comment->comment}}</h5>
				<h6 class="text-end">posted by: {{$comment->user->name}}</h6>
				<h6 class="text-end mt-3">posted on: {{$comment->created_at}}</h6>
			</div>
		</div>
	@endforeach
@endsection